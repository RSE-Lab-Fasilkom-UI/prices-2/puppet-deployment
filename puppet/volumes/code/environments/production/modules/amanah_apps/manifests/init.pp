class amanah_apps{
# Configure postgresql
  class { 'postgresql::server':
    postgres_password => 'spleliveid',
    before => File['/tmp/digital_vm_databases.sql']
  }

  file { '/tmp/digital_vm_databases.sql':
    ensure => present,
    source => 'puppet:///modules/amanah_apps/database/digital_vm_databases.sql',
    source_permissions => 'use',
    before => Exec['import_digital_ocean_databases'],
  }

  exec { 'import_digital_ocean_databases':
    command => 'sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && locale-gen && psql -h localhost -U postgres -f /tmp/digital_vm_databases.sql',
    onlyif => 'test -f /tmp/digital_vm_databases.sql',
    path    => ['/usr/local/bin', '/usr/bin'],
    environment => [
      'PGPASSWORD=spleliveid',
    ],
    timeout     => 6000,
    returns => [0, 2],
  }

# Create groups and owners
  exec { 'create_rse_group':
    command => 'groupadd rse',
    unless  => 'getent group rse',
    path    => ['/usr/local/bin', '/usr/bin', '/bin', '/usr/sbin'],
  }

  exec { 'create_www_data_owner':
    command => 'useradd -r www-data',
    unless  => 'id www-data',
    path    => ['/usr/local/bin', '/usr/bin', '/bin', '/usr/sbin'],
  }
# File extraction
  file { "/tmp/old-server-files.tar.gz":
    source => "puppet:///modules/amanah_apps/applications/old-server-files.tar.gz",
    before => Exec['extract_archive'],
  }

  exec { "extract_archive":
    command => "tar -zxvf /tmp/old-server-files.tar.gz",
    path    => '/usr/bin:/bin',
    timeout     => 6000,
    cwd     => '/tmp',
  }

  exec { 'move_html':
    command => 'rsync -a --ignore-times /tmp/var/www/html/ /var/www/html/',
    path    => ['/usr/local/bin', '/usr/bin', '/bin', '/usr/sbin'],
    require => Exec['extract_archive'],
    timeout     => 6000,
    user        => 'root',
  }

  exec { 'move_engine':
    command => 'rsync -a --ignore-times /tmp/var/www/engine/ /var/www/engine/',
    path    => ['/usr/local/bin', '/usr/bin', '/bin', '/usr/sbin'],
    require => Exec['extract_archive'],
    timeout     => 6000,
    user        => 'root',
  }

  exec { 'move_monitoring':
    command => 'rsync -a --ignore-times /tmp/var/www/monitoring/ /var/www/monitoring/',
    path    => ['/usr/local/bin', '/usr/bin', '/bin', '/usr/sbin'],
    require => Exec['extract_archive'],
    timeout     => 6000,
    user        => 'root',
  }

  exec { 'move_productlines':
    command => 'rsync -a --ignore-times /tmp/var/www/productlines/ /var/www/productlines/',
    path    => ['/usr/local/bin', '/usr/bin', '/bin', '/usr/sbin'],
    require => Exec['extract_archive'],
    timeout     => 6000,
    user        => 'root',
  }

  exec { 'move_products':
    command => 'rsync -a --ignore-times /tmp/var/www/products/ /var/www/products/',
    path    => ['/usr/local/bin', '/usr/bin', '/bin', '/usr/sbin'],
    require => Exec['extract_archive'],
    timeout     => 6000,
    user        => 'root',
  }

  exec { 'move_add_ssl_cert.sh':
    command => 'rsync -a --ignore-times /tmp/var/www/add_ssl_cert.sh /var/www/',
    path    => ['/usr/local/bin', '/usr/bin', '/bin', '/usr/sbin'],
    require => Exec['extract_archive'],
    timeout     => 6000,
    user        => 'root',
  }

  exec { 'move_remove_ssl_cert.sh':
    command => 'rsync -a --ignore-times /tmp/var/www/remove_ssl_cert.sh /var/www/',
    path    => ['/usr/local/bin', '/usr/bin', '/bin', '/usr/sbin'],
    require => Exec['extract_archive'],
    timeout     => 6000,
    user        => 'root',
  }

  exec { 'move_fix_owner_and_permission.sh':
    command => 'rsync -a --ignore-times /tmp/var/www/fix_owner_and_permission.sh /var/www/',
    path    => ['/usr/local/bin', '/usr/bin', '/bin', '/usr/sbin'],
    require => Exec['extract_archive'],
    timeout     => 6000,
    user        => 'root',
  }

  exec { 'move_index.nginx-debian.html':
    command => 'rsync -a --ignore-times /tmp/var/www/index.nginx-debian.html /var/www/',
    path    => ['/usr/local/bin', '/usr/bin', '/bin', '/usr/sbin'],
    require => Exec['extract_archive'],
    timeout     => 6000,
    user        => 'root',
  }

  exec { 'move_nginx':
    command => 'rsync -a --ignore-times /tmp/etc/nginx/ /etc/nginx/',
    path    => ['/usr/local/bin', '/usr/bin', '/bin', '/usr/sbin'],
    require => Exec['extract_archive'],
    timeout     => 6000,
    user        => 'root',
  }

# Install Engines
  exec { 'pip-install-admin':
    command => 'python3.8 -m pip install -r /var/www/engine/admin/requirements.txt',
    path    => ['/usr/local/bin', '/usr/bin', '/bin'],
    require => Exec['move_engine'],
    user        => 'root',
  }

  exec { 'pip-install-cli':
    command => 'python3.8 -m pip install -r /var/www/engine/cli/requirements.txt',
    path    => ['/usr/local/bin', '/usr/bin', '/bin'],
    require => Exec['move_engine'],
    user        => 'root',
  }

  exec { 'pip-install-ifml':
    command => 'python3.8 -m pip install -r /var/www/engine/ifml/requirements.txt',
    path    => ['/usr/local/bin', '/usr/bin', '/bin'],
    require => Exec['move_engine'],
    user        => 'root',
  }

# Install monitoring
  exec { 'pip-install-monitoring':
    command => 'python3.8 -m pip install -r /var/www/monitoring/requirements.txt',
    path    => ['/usr/local/bin', '/usr/bin', '/bin'],
    require => Exec['move_monitoring'],
    user        => 'root',
  }

# Test File and install package
  file { "/tmp/test_file.txt":
    source => "puppet:///modules/amanah_apps/applications/test_file.txt",
    before => Exec['extract_archive'],
  }
  package { ['vim']:
      ensure => 'installed',
  }
}

