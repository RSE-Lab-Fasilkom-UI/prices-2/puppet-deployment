class systems {
# Tools needed
 package { ['git', 'make', 'curl', 'wget', 'sudo', 'rsync', 'locales']:
    ensure => 'installed',
  }

  include java
  include python
  include php
  include js
  include amanah_apps
  Class['java'] -> Class['python'] -> Class['php'] -> Class['js'] -> Class['amanah_apps']

}
