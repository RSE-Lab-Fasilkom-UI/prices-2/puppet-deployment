class js {
    package { ['npm', 'nodejs']:
        ensure => 'installed',
    }

    exec { 'install-yarn':
      command => 'npm install --global yarn',
      path    => '/usr/bin:/bin',
    }
}
