# Copyright (c) 2010 Curt Micol <asenchi@asenchi.com>
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED 'AS IS' AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

# Puppet manifest for installing the latest releases of Python.

# This library will install the following versions of Python:
#   3.8.9
#
# Each version is configurable by setting $version in python::v(major,minor)
#
# We also install distribute and pip in versions 2.[5,6]. I haven't
# experimented with 3.1 enough to know whether both pip and distribute work.

# Will be adding a 'packages.pp' manifest for installing different packages
# using pip.

class python::source {
  define install($tarball, $tmpdir, $flags) {

# Install prequisite
    package { ["build-essential", "zlib1g-dev", "libncurses5-dev", "libgdbm-dev", "libnss3-dev", "libssl-dev", "libreadline-dev", "libffi-dev"]:
        ensure => 'installed',
    }

# Get Tar
    exec { "retrieve-$name":
      command => "wget $tarball",
      cwd => "$tmpdir",
      before => Exec["extract-$name"],
      notify => Exec["extract-$name"],
      timeout     => 6000,
    }

# Extract Tar
    exec { "extract-$name":
      command => "tar -zxf $name.tgz",
      cwd => $tmpdir,
      creates => "$tmpdir/$name/README",
      require => Exec["retrieve-$name"],
      before => Exec["configure-$name"],
      timeout     => 6000,
    }

# Confugre
    exec { "configure-$name":
      cwd => "$tmpdir/$name",
      command => "$tmpdir/$name/configure $flags",
      require => Exec["extract-$name"],
      before => Exec["make-$name"],
      timeout     => 6000,
    }

# Install using make install
    exec { "make-$name":
      cwd => "$tmpdir/$name",
      command => "make && make install",
      require => Exec["configure-$name"],
      timeout     => 6000,
    }
  }
}

class python {
  $tmpdir = "/tmp/python"

  Exec { path => "/usr/bin:/bin:/usr/sbin:/sbin" }
  file { ["$tmpdir"]: ensure => directory }


  include python::source

  # Install python 3.8.9
  $version = "3.8.9"
  python::source::install { "Python-$version":
    tarball => "http://python.org/ftp/python/$version/Python-$version.tgz",
    tmpdir => $tmpdir,
    flags => "",
  }

}
