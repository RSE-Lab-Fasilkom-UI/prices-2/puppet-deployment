class php::source {
  include apt

  define install{

  # Prequisites
    package { ['gnupg2', 'ca-certificates', 'lsb-release', 'apt-transport-https', 'software-properties-common']:
        ensure => 'installed',
    }

    exec { 'add-apt-repository':
      command => 'add-apt-repository -y ppa:ondrej/php && apt update -y',
      path    => '/usr/bin:/bin',
      require => Package['software-properties-common'],
    }

    package { ["php7.2", "libapache2-mod-php7.2", "php7.2-common", "php7.2-curl", "php7.2-mbstring", "php7.2-xmlrpc", "php7.2-pgsql", "php7.2-gd", "php7.2-xml", "php7.2-intl", "php7.2-ldap", "php7.2-imagick", "php7.2-json", "php7.2-cli", "php7.2-zip", "ant", "nginx"]:
        ensure => 'installed',
    }

  }


}
class php {
  include php::source

  php::source::install{ "PHP 7.2": }
}
