node default {
  exec { 'update-package-list':
    command => 'apt update -y',
    path    => '/usr/bin:/bin',
    before => Class['java'],
  }

  include systems

}
