#!/bin/bash

# NOTE: The installation method for java and php are different for Ubuntu and Debian

# Python versions to be installed
PYVERSIONS=(
	"3.8.9"
)

pre_task() {
	echo "================================="
	echo "======== Running pre-task ======="
	echo "================================="

	#
	# Get information about the latest packages
	apt-get update -y
	# Upgrade system
	apt upgrade -y

	apt install -y git make curl wget sudo rsync
}

install_python() {
	echo "================================="
	echo "===== Installing Python 3.8 ====="
	echo "================================="

	# Python 3.7
	# Install build tools for python
	apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget -y

	## Create directory for build packages
	mkdir -p packages
	cd packages || exit

	## Install all python versions listed on PYVERSIONS
	for pyver in "${PYVERSIONS[@]}"; do
		echo "======$pyver======"
		# Download tar.gz, extract it, and enter the directory
		wget https://www.python.org/ftp/python/"$pyver"/Python-"$pyver".tgz
		tar xzf Python-"$pyver".tgz
		cd Python-"$pyver" || exit

		# Configure, build, and install the python package
		./configure
		make
		make install

		# Return to the parent directory for further installation
		cd ..

	done
	## Install pip for python3
	apt install python3-pip -y
}

install_java() {
	echo "======= Installing Java 8 ======="

	apt-get install openjdk-8-jdk -y
}

install_php() {
	echo "================================="
	echo "======= Installing PHP 7.2 ======"
	echo "================================="

	apt install apt-transport-https ca-certificates curl software-properties-common -y

	## Ubuntu 22
	apt-add-repository ppa:ondrej/php -y

	# Get information about the latest packages
	apt-get update -y

	apt install tzdata -y
	ln -fs /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
	dpkg-reconfigure -f noninteractive tzdata

	# Install php and it's common extensions
	apt-get install -y php7.2 libapache2-mod-php7.2 php7.2-common php7.2-curl php7.2-mbstring php7.2-xmlrpc php7.2-pgsql php7.2-gd php7.2-xml php7.2-intl php7.2-ldap php7.2-imagick php7.2-json php7.2-cli php7.2-zip ant php7.2-fpm nginx

}

install_npm() {

	apt install nodejs npm -y
	npm install --global yarn

}

install_postgres() {
	# Install packages
	apt install postgresql postgresql-contrib locales -y

	# Generate locale
	sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && locale-gen
}

file_transfer() {
	export PSQL_PASSWORD=spleliveid

	# Create owners and group
	groupadd rse
	useradd -r www-data

	# Unpack and file transfer
	apt install rsync
	cd /tmp || exit
	tar -zxvf /tmp/old-server-files.tar.gz

	sudo -u postgres /usr/bin/psql -f /tmp/digital_vm_databases.sql
	sudo /usr/bin/psql -U postgres -c "ALTER USER postgres WITH PASSWORD '$PSQL_PASSWORD';"

	# Transfer file
	rsync -a --ignore-times /tmp/var/www/html/ /var/www/html/
	rsync -a --ignore-times /tmp/var/www/engine/ /var/www/engine/
	rsync -a --ignore-times /tmp/var/www/monitoring/ /var/www/monitoring/
	rsync -a --ignore-times /tmp/var/www/productlines/ /var/www/productlines/
	rsync -a --ignore-times /tmp/var/www/products/ /var/www/products/
	rsync -a --ignore-times /tmp/var/www/add_ssl_cert.sh /var/www/
	rsync -a --ignore-times /tmp/var/www/remove_ssl_cert.sh /var/www/
	rsync -a --ignore-times /tmp/var/www/fix_owner_and_permission.sh /var/www/
	rsync -a --ignore-times /tmp/var/www/index.nginx-debian.html /var/www/
	rsync -a --ignore-times /tmp/etc/nginx/ /etc/nginx/

	# Install python packages
	python3.8 -m pip install -r /var/www/engine/admin/requirements.txt
	python3.8 -m pip install -r /var/www/engine/cli/requirements.txt
	python3.8 -m pip install -r /var/www/engine/ifml/requirements.txt
	python3.8 -m pip install -r /var/www/monitoring/requirements.txt

}

script_run() {
	pre_task
	install_python
	install_java
	install_php
	install_npm
	install_postgres
	file_transfer
}

export DEBIAN_FRONTEND=noninteractive
script_run
apt install vim -y
