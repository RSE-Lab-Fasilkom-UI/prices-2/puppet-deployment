# Disclaimer
- If you see this and have errors regarding packages please check if it's still supported by Ubuntu 22.04 (as this was created with this machine as it's premise)
- Please also note that packages may disappear due to the passage of time just like how my money disappear ᕕ( ᐛ )ᕗ
- Don't forget to pray to buff the chances of you not encountering errors as I did (つ▀¯▀ )つ
- Stay hydrated and well fed to prevent any dumb mistakes (｡•̀ᴗ-)✧

# Notes

## Digital Ocean VM

### Files

- /var/www/productlines/aisco -> Web page
- /var/www/engine/cli -> App generator yang muncul di /var/www/products
- /var/www/engine/fix_owner_and_permission.sh -> Script for update permission
- /var/www/engine/abs -> backend
- /var/www/engine/ifml -> generator frontend
- /var/www/engine/admin -> admin page & static page
- /var/www/monitoring -> monitoring
- /var/www/productlines/aiso -> profile website amanah
- /var/www/html -> halaman depan SPLE


### Deployments

#### Key Notes

- No need for virtual environments, just bare metal, everything is already adjusted
- Backup is located at ./puppet/volumes/code/environments/production/modules/amanah_apps/files
  - Database consist of the backup data while the applications consist of file archive and test file to be tested

##### Accessing VM

1. SSH to VM pacil (prod and prices2) needs to be forwarded through kawung
   E.X: local -> kawung -> prices2
    - Easy way is to use port forward by adding below to ~/.ssh/config.

  ```bash
  # Change accordingly

  # Access to Kawung
  Host affan_kawung.cs.ui.ac.id
  User ichlasul.affan
  HostName kawung.cs.ui.ac.id
  Port 12122
  IdentityFile ~/.ssh/id_rsa
# For tunneling
  ForwardAgent yes
  AddressFamily inet
  ForwardX11Trusted yes

  # Access to VM pacil

  # prices-staging2
  Host vm_pacil_baru
      User krishertanto
      HostName 10.119.106.130
      IdentityFile ~/.ssh/id_rsa
      # For tunneling
      ForwardAgent yes
      ProxyJump affan_kawung.cs.ui.ac.id

  # prices
  Host vm_pacil_prod
      User krishertanto
      HostName 152.118.148.65
      IdentityFile ~/.ssh/id_rsa
      # For tunneling
      ForwardAgent yes
      ProxyJump affan_kawung.cs.ui.ac.id
  ```

##### Programs Versions

###### OS

```lang
NAME="Ubuntu"
VERSION="16.04.6 LTS (Xenial Xerus)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 16.04.6 LTS"
VERSION_ID="16.04"
HOME_URL="http://www.ubuntu.com/"
SUPPORT_URL="http://help.ubuntu.com/"
BUG_REPORT_URL="http://bugs.launchpad.net/ubuntu/"
VERSION_CODENAME=xenial
UBUNTU_CODENAME=xenial

```

###### Java

```lang
openjdk version "1.8.0_275"
OpenJDK Runtime Environment (build 1.8.0_275-8u275-b01-0ubuntu1~16.04-b01)
OpenJDK 64-Bit Server VM (build 25.275-b01, mixed mode)
```

###### PHP

```lang
PHP 7.2.27-6+ubuntu16.04.1+deb.sury.org+1 (cli) (built: Feb  5 2020 16:52:09) ( NTS )
Copyright (c) 1997-2018 The PHP Group
Zend Engine v3.2.0, Copyright (c) 1998-2018 Zend Technologies
with Zend OPcache v7.2.27-6+ubuntu16.04.1+deb.sury.org+1, Copyright (c) 1999-2018, by Zend Technologies
```


###### Python

```lang
Python 2.7.12
Python 3.7.9
```



#### Files to be transferred

- /var/www/engine
- /var/www/monitoring
- /var/www/productlines

#### Cheatsheet

##### Add packages repository without software-properties-common (apt-add-repository)

```bash
echo '[packages link]' 
# Example to add repository, as well as it's key
# echo 'deb https://ppa.launchpadcontent.net/ondrej/php/ubuntu xerial main ' >> /etc/apt/sources.list
# echo '#deb-src https://ppa.launchpadcontent.net/ondrej/php/ubuntu xerial main ' >> /etc/apt/sources.list
# sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4f4ea0aae5267a6c 
```

##### Login to ubuntu root

```bash
sudo bash 
```

## Milestones

### Deployment (Manual)

#### Copying
- Use the steps listed in ./setup_script/analysis_testing
- The scripts listed in the script ./setup_script/install_requirements and ./setup_script/ are old versions that may come in handy in case needed

#### Generating using /var/engine/cli
- Follow the steps listed on [Generate application](###generate-application)
- Things to note
  - If an application with the same name already exist change name or destroy the application first
  - Don't use hypen (-) in urls it will cause error when destroying applications
  - There will be errors caused by sending emails, ignore it and focus onto the next stage

#### Testing nginx
- Check process `ps aux | grep [application name]` to make sure application is running
- Check website
### Puppet automation

# Troubleshooting (So you won't suffer as I did (づ ◕‿◕ )づ)

## Unable to SSH

### Digital Ocean VM

#### Port (22422) is blocked by ISP (Indihome) (╯°□°）╯︵ ┻━┻

- Use other internet provider (cellular data works)
- Unblock port from ISP (not quite clear if it violates terms and conditions)

### VM Pacil

- Need to have access to kawung.cs.ui.ac.id


## Installation Script (Created)

### OS Issues

#### Debian 10

##### Unable to install packages or force to do cd rom

- Modify /etc/apt/sources.list from 
```bash
deb cdrom:[Debian GNU/Linux 10.0.0 _Buster_ - Official amd64 DVD Binary-1 2$

deb cdrom:[Debian GNU/Linux 10.0.0 _Buster_ - Official amd64 DVD Binary-1 2$

deb http://security.debian.org/debian-security buster/updates main contrib
deb-src http://security.debian.org/debian-security buster/updates main contrib

# buster-updates, previously known as 'volatile'
# A network mirror was not selected during install.  The following entries
# are provided as examples, but you should amend them as appropriate
# for your mirror of choice.
#
# deb http://deb.debian.org/debian/ buster-updates main contrib
 #deb-src http://deb.debian.org/debian/ buster-updates main contrib
```
  to 
```bash
# deb cdrom:[Debian GNU/Linux 10.0.0 _Buster_ - Official amd64 DVD Binary-1 20190706-10:24]/ buster contrib main

# deb cdrom:[Debian GNU/Linux 10.0.0 _Buster_ - Official amd64 DVD Binary-1 20190706-10:24]/ buster contrib main

deb http://security.debian.org/debian-security buster/updates main contrib
deb-src http://security.debian.org/debian-security buster/updates main contrib

# buster-updates, previously known as 'volatile'
# A network mirror was not selected during install.  The following entries
# are provided as examples, but you should amend them as appropriate
# for your mirror of choice.
#
deb http://deb.debian.org/debian/ buster buster-updates main contrib
deb-src http://deb.debian.org/debian/ buster buster-updates main contrib
```
  TL;DR: Comment `deb cdrom` and uncomment the last two lines as well as
  add `buster` before `buster-updates`

##### Unable to install openjdk8

Do one of the following:
- Add mirror with openjdk8 (from stretch) may be dangerous to mixing mirrors
```bash
apt-add-repository 'deb http://security.debian.org/debian-security stretch/updates main'
apt-get update
apt-get install openjdk-8-jdk
```
- Use adoptopenjdk repository
```bash
wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | sudo apt-key add -
sudo add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/
sudo apt-get update && sudo apt-get install adoptopenjdk-8-hotspot
```

## Generating /var/www/engine/cli (Assuming has already copied the files from .tar.gz)

### Generate application
- Things to note:
  - The flags `--subdomain` is used for the subdomain of application (no hypen)
  - If there is an error due to permission modified /var/www/engine/cli/roles.sample.json and /var/www/engine/cli/user_data.sample.json
  - Generating the appplication itself doesn't take too long (10 minutes tops) but making the website custom made after configuring it to the browser takes quite a while so touch some grass first 

#### Generate app using TestOld.conf file

##### Using google login

```bash
# NOTE: TestOld is the application name change accordingly

sudo python3 runner.py -cfg config.ini deploy -mnt -ssl -auth "google" --auth-id "741486500949-v8v5cdc0gfjoa3c9vpofb5cjtt8hej9c.apps.googleusercontent.com" -fe "angular" --roles-file "/var/www/engine/cli/roles.sample.json" --subdomain testold.amanah-old.cs.ui.ac.id TestOld "/var/www/engine/cli/Products.sample.abs" "/var/www/engine/cli/user_data.sample.json"
```

##### Using Auth0

```bash
# NOTE: TestOld is the application name change accordingly

sudo python3 runner.py -cfg config.ini deploy -mnt -ssl -auth "auth0" --auth-id "ichlaffterlalu.au.auth0.com" --auth-secret "ED4753GKzpgdvb7sz2pSm9RwwovPTU3q" --roles-file "/var/www/engine/cli/roles.sample.json" --subdomain testold TestOld "/var/www/engine/cli/Products.sample.abs" "/var/www/engine/cli/user_data.sample.json" 
```

#### Destroy

```bash
# NOTE: TestOld is the application name change accordingly

sudo python3 runner.py -cfg config.ini destroy TestOld
```

#### Certificate SSL

```bash
# Current machine
sudo /var/www/add_ssl_cert.sh testold testold.amanah-old.cs.ui.ac.id

# Prod machine change the 2 value after -J accordingly
sudo ssh -i /root/.ssh/id_rsa -J ichlasul.affan@kawung.cs.ui.ac.id:12122 krishertanto@152.118.148.65 "sudo /var/www/add_ssl_cert.sh testold testold.amanah-old.cs.ui.ac.id"
```
- Make sure it is generated on both server (production and staging)

##### SSL on both production and staging automation

###### Flow

python3 runner.py deploy -> build app -> fix permission -> SSL on staging -> SSH to production -> SSL on production

#### Generating /var/www/engine/cli

##### Check errors or not

###### \/var/www/products/log_backup/

- Check here to see for logs

###### \/var/www/products/TestOld/

- If error occur specifying the file /var/www/products/nginx/TestOld.conf due to the file not exisitng you can create an empty file
```bash
sudo touch /var/www/products/nginx/TestOld.conf
```
- Location of the generated apps
                    - Take a look at logs/ and `cat` each logs to and check for theme

###### Key notes

- Java will emit errors regardless if it suceeds, just take a look at
  the last line

##### Postgresql won't run

###### Can't run PID 

```bash
# Start service
sudo service postgresql restart
```

##### Missing dependencies

- Install nodejs, yarn, ant, npm

##### Used react instead of angular for old version

- Edit \/var/www/engine/ifml/__init__.py

##### Error in SSL

- Change /var/www/engine/cli/config.ini and change things under \[general\]

##### 403 Forbidden

Run the following
```bash
/var/www/fix_owner_and_permission.sh
```

##### Permission denied when using features

- Check /var/www/engine/cli/user_data.samples.json and change email
- Add email to  /var/www/engine/cli/roles.samples.json

#### Automation Script

##### SSH to prices from prices2 

```bash
sudo ssh -i /root/.ssh/id_rsa -J ichlasul.affan@kawung.cs.ui.ac.id:12122 krishertanto@152.118.148.65
```      
  

## Nginx and certificates

#### PHP Aisco

##### Change to nginx

- go to /etc/nginx/sites-available/amanah-old.conf
    - Change root aisco 
    - Change index from index debian.html to index.php

#### Automated certificate for both server

##### Changelog

- /var/www/engine/cli/deployment/refresh_nginx.py -> line 15 add args as parameter
- /var/www/engine/cli/deployment/deploy.py -> line 28 add args
- /var/www/engine/cli/components/nginx.py
    - Line 57 & line 58; add method
    - Permission fix in order to fix permissions for files to avoid unauthorized when accessing files
    - ssl_cerif_script_run run the script for certification in /var/www/add_ssl_cert.sh

## Puppet

### Architecture

- Agent -> Ask Master -> Master check config -> Master return response to Agent and apply changes

### Setting Up

#### Unlocking repository

```bash
sudo apt-get update -y 
sudo apt install wget

# Can change version, check https://www.puppet.com/docs/puppet/7/install_puppet.html#enable_the_puppet_platform_repository
wget https://apt.puppetlabs.com/puppet7-release-jammy.deb
sudo dpkg -i puppet7-release-jammy.deb
```

#### Installing Puppet Server

```bash
sudo apt update -y
sudo apt install puppetserver -y
sudo apt-get install puppetserver -y

sudo systemctl start puppet-server
sudo systemctl enable puppetserver
```

#### Installing Puppet Agent

```bash
sudo apt update -y
sudo apt install puppet-agent -y
sudo apt-get install puppet-agent -y

sudo systemctl start puppet
sudo systemctl enable puppet

```

#### Adding Agents


### Quicknotes

#### Using Docker as a puppet server

- Create a docker container using a the image in puppet/puppetserver
    - For the first time only uncomment the code part in order to generate the others. Only after the others are copied can the others be uncommented, otherwise there will be ssl issues

##### Configure Agent

###### VM and docker container are in the same VM

- Configure DNS in /etc/hosts
        - Point it to the such IP address of the container as well as it's hostname
```bash
docker inspect [container] | grep IPAddress] [hostname listed in docker compose]
```
- Configure /etc/puppetlabs/puppet/puppet.conf
```bash
[main]
server = [hostname]
certname = [agent certname]
```

#### Cheat Code to skip headache setting up Puppet
- Just use Docker Compose that already exist ( ๑‾̀◡‾́)σ"
